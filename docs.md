![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/gigante/desafio/master) [![codecov](https://codecov.io/bb/gigante/desafio/branch/master/graph/badge.svg)](https://codecov.io/bb/gigante/desafio)

# Desafio Orama

## How to install/use

Web UI is like DRF

## Local

```bash
$ make install
$ make run
```

**To access**: [http://localhost:5000](http://localhost:5000)

## Docker

```bash
$ make deploy
```

**To access**: [http://localhost:8000](http://localhost:8000)

## Heroku

**To access**: [https://skillsummary.herokuapp.com/](https://skillsummary.herokuapp.com/)


## Tests

Note: Tests and coverage run without docker

Run tests

```
$ make test
```

Coverage report

```
$ make coverage
```