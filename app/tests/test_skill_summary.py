import unittest
import json
from pathlib import Path
from app.skill_summary import SkillSummary
from app.date_calc import d
from api import app as api


class SkillSummaryTest(unittest.TestCase):
    json_result = '{"freelance": {"id": 42, "computedSkills": [{"id": 241, "name": "React", "durationInMonths": 28}, {"id": 270, "name": "Node.js", "durationInMonths": 28}, {"id": 370, "name": "Javascript", "durationInMonths": 60}, {"id": 470, "name": "MySQL", "durationInMonths": 32}, {"id": 400, "name": "Java", "durationInMonths": 40}]}}'

    def setUp(self):
        data = self.get_json('examples/freelancer.json')
        self.summary = SkillSummary(data)

    def get_json(self, json_file):
        file = Path(f'{json_file}').absolute()

        with open(file, "r") as read_file:
            return json.load(read_file)

    def testSummary(self):
        self.assertEqual(
            json.dumps(self.summary.get()),
            self.json_result
        )

    def testFixOverlap(self):
        fixed = self.summary.fix_overlap()
        ref = {
            241: {'name': 'React', 'dates': [{
                'ini': d('2016-01-01T00:00:00+01:00'),
                'end': d('2018-05-01T00:00:00+01:00')
            }]},
            270: {'name': 'Node.js', 'dates': [{
                'ini': d('2016-01-01T00:00:00+01:00'),
                'end': d('2018-05-01T00:00:00+01:00')
            }]},
            370: {'name': 'Javascript', 'dates': [{
                'ini': d('2013-05-01T00:00:00+01:00'),
                'end': d('2018-05-01T00:00:00+01:00')}
            ]},
            470: {'name': 'MySQL', 'dates': [{
                'ini': d('2014-01-01T00:00:00+01:00'),
                'end': d('2016-09-01T00:00:00+01:00')}
            ]},
            400: {'name': 'Java', 'dates': [{
                'ini': d('2013-05-01T00:00:00+01:00'),
                'end': d('2016-09-01T00:00:00+01:00')
            }]}
        }
        self.assertEqual(fixed, ref)

    def testApiValidJson(self):
        app = api.test_client()
        payload = self.get_json('examples/freelancer.json')
        response = app.post('/', headers={"Content-Type": "application/json"}, data=json.dumps(payload))
        self.assertEqual(200, response.status_code)

    def testApiInvalidJson(self):
        app = api.test_client()
        payload = self.get_json('exercise/freelancer.json')
        response = app.post('/', headers={"Content-Type": "application/json"}, data=json.dumps(payload))
        self.assertEqual(422, response.status_code)


