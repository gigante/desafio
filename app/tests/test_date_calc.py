import unittest
from app.date_calc import d, count_months, latest, oldest, overlap


class CalcDateTest(unittest.TestCase):
    def test_count_months1(self):
        ini = d('2016-01-01T00:00:00+01:00')
        end = d('2018-05-01T00:00:00+01:00')
        self.assertEqual(count_months(ini, end), 28)

    def test_count_months2(self):
        ini = d('2009-12-01T00:00:00+01:00')
        end = d('2020-09-01T00:00:00+01:00')
        self.assertEqual(count_months(ini, end), 129)

    def test_count_months3(self):
        ini = d('2020-05-01T00:00:00+01:00')
        end = d('2020-10-01T00:00:00+01:00')
        self.assertEqual(count_months(ini, end), 5)

    def test_overlap1(self):
        ini1 = d('2020-01-01T00:00:00+01:00')
        end1 = d('2020-06-01T00:00:00+01:00')
        ini2 = d('2020-04-01T00:00:00+01:00')
        end2 = d('2020-07-01T00:00:00+01:00')
        self.assertEqual(overlap(ini1, end1, ini2, end2), True)

    def test_overlap2(self):
        ini1 = d('2020-04-01T00:00:00+01:00')
        end1 = d('2020-07-01T00:00:00+01:00')
        ini2 = d('2020-01-01T00:00:00+01:00')
        end2 = d('2020-06-01T00:00:00+01:00')
        self.assertEqual(overlap(ini1, end1, ini2, end2), True)

    def test_overlap3(self):
        ini1 = d('2020-01-01T00:00:00+01:00')
        end1 = d('2020-10-01T00:00:00+01:00')
        ini2 = d('2020-03-01T00:00:00+01:00')
        end2 = d('2020-07-01T00:00:00+01:00')
        self.assertEqual(overlap(ini1, end1, ini2, end2), True)

    def test_overlap4(self):
        ini1 = d('2020-03-01T00:00:00+01:00')
        end1 = d('2020-07-01T00:00:00+01:00')
        ini2 = d('2020-01-01T00:00:00+01:00')
        end2 = d('2020-10-01T00:00:00+01:00')
        self.assertEqual(overlap(ini1, end1, ini2, end2), True)

    def test_not_overlapped(self):
        ini1 = d('2019-03-01T00:00:00+01:00')
        end1 = d('2019-09-01T00:00:00+01:00')
        ini2 = d('2020-01-01T00:00:00+01:00')
        end2 = d('2020-10-01T00:00:00+01:00')
        self.assertEqual(overlap(ini1, end1, ini2, end2), False)

    def test_latest(self):
        d1 = d('2020-06-01T00:00:00+01:00')
        d2 = d('2020-07-01T00:00:00+01:00')
        self.assertEqual(latest(d1, d2), d2)

    def test_oldest(self):
        d1 = d('2020-01-01T00:00:00+01:00')
        d2 = d('2020-04-01T00:00:00+01:00')
        self.assertEqual(oldest(d1, d2), d1)
