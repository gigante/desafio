import json
from app.date_calc import d, overlap, oldest, latest, count_months


class SkillSummary:
    def __init__(self, data):
        self.id = data['freelance']['id']
        self.experiences = data['freelance']['professionalExperiences']

    def get(self):
        summary = self.fix_overlap()
        return self.count_skill_months(summary)

    def fix_overlap(self):
        summary = {}  # organized skills
        for job in self.experiences:
            ini = d(job['startDate'])
            end = d(job['endDate'])
            overlapped = False

            for skill in job['skills']:
                skill_id = skill['id']
                if skill_id not in summary:
                    summary[skill_id] = {'name': skill['name'], 'dates': []}

                # verify if overlap other dates
                dates = summary[skill_id]['dates']
                for dt in dates:
                    overlapped = overlap(dt['ini'], dt['end'], ini, end)
                    if overlapped:
                        dt['ini'] = oldest(dt['ini'], ini)
                        dt['end'] = latest(dt['end'], end)
                        break

                if not overlapped:
                    dates.append({'ini': ini, 'end': end})

        return summary

    def count_skill_months(self, summary):
        result = {'freelance': {
            'id': self.id,
            'computedSkills': []
        }}

        for key, val in summary.items():
            months = 0
            for dt in val['dates']:
                months += count_months(dt['ini'], dt['end'])

            result['freelance']['computedSkills'].append({
                'id': key,
                'name': val['name'],
                'durationInMonths': months
            })

        return result
