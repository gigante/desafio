from datetime import datetime


def d(dt):
    return datetime.fromisoformat(dt)


def count_months(ini, end):
    months = (end.year - ini.year) * 12
    return months + end.month - ini.month


def latest(d1, d2):
    return d1 if d1 > d2 else d2


def oldest(d1, d2):
    return d1 if d1 < d2 else d2


def overlap(ini1, end1, ini2, end2):
    return ini1 < end2 and end1 > ini2
