import json
from flask import request
from flask_api import FlaskAPI, status
from app.skill_summary import SkillSummary

app = FlaskAPI(__name__)
example = {
	"freelance": {
    	"id": 42
    }
}

@app.route('/', methods=['GET', 'POST'])
def skill_summary():
    if request.method == 'POST':
        try:
            summary = SkillSummary(request.data)
            return {'data': summary.get()}, 200
        except:
            content = {'Client error': 'unable to be processed.'}
            return content, 422

    return {'data': example}


if __name__ == "__main__":
    app.run(debug=True)
