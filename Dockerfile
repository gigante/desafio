FROM python:3.8.5-alpine
WORKDIR /app
COPY . .
RUN pip install -r requirements.txt
EXPOSE 8000
CMD ["gunicorn", "api:app", "--bind", "0.0.0.0:8000", "--workers=3"]