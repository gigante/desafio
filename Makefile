help:
	@echo
	@echo ".   ,     ,             .      "
	@echo "|\ /|     |        ,- o |      "
	@echo "| V | ,-: | , ,-.  |  . | ,-.  "
	@echo "|   | | | |<  |-'  |- | | |-'  "
	@echo "'   ' '-' ' \` '-'  |  ' ' '-' "
	@echo "                  -'           "
	@echo "Available target rules"
	@echo
	@echo "install   install dependencies"
	@echo "run       run app"
	@echo "test      run unit tests"
	@echo "coverage  coverage report"
	@echo "-----"
	@echo "deploy    run app with docker"

install:
	@pip install -r requirements.txt

run:
	@gunicorn api:app --bind 0.0.0.0:5000 --reload

test:
	@python -m unittest discover -s app/tests -v

coverage:
	@coverage run -m unittest discover -s app/tests
	@coverage report -m

deploy:
	@docker build -t skill-app .
	@docker run -d -p 8000:8000 --name app skill-app